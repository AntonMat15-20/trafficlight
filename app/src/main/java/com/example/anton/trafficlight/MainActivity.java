package com.example.anton.trafficlight;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    private ConstraintLayout mainLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainLayout = findViewById(R.id.mainLayout);

        Button yellowButton = findViewById(R.id.YellowButton);
        yellowButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mainLayout.setBackgroundColor(getResources().getColor(R.color.colorYellow));
            }
        });
        Button redButton = findViewById(R.id.RedButton);
        redButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mainLayout.setBackgroundColor(getResources().getColor(R.color.colorRed));
            }
        });
        Button greenButton = findViewById(R.id.GreenButton);
        greenButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mainLayout.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
        });
    }
}
